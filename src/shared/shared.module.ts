import { environment } from './../environments/environment';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { ProfilePictureComponent } from './components/profile-picture/profile-picture.component';
import { AngularFireModule } from '@angular/fire';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { AngularFireStorageModule } from '@angular/fire/storage';
import { CommonModule } from '@angular/common';
@NgModule({
    providers: [],
    imports: [
        CommonModule,
        BrowserModule,
        AngularFireModule.initializeApp(environment.firebase),
        AngularFirestoreModule,
        AngularFireStorageModule
    ],
    declarations: [ProfilePictureComponent],
    exports: [ProfilePictureComponent, AngularFireModule, AngularFirestoreModule, AngularFireStorageModule]
})
export class SharedModule { }
