export class User {
    username: string;
    email: string;
    gender: string;
    bio: string;

    constructor(username = '', email = '', gender = '', bio = '') {
        this.username = username;
        this.email = email;
        this.gender = gender;
        this.bio = bio;
    }
}
