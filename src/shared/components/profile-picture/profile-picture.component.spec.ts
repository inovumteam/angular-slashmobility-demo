import { AngularFireStorage } from '@angular/fire/storage';
import { StorageService } from './../../../core/services/storage/storage.service';
// tslint:disable
import { TestBed } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ProfilePictureComponent } from './profile-picture.component';
import { BehaviorSubject } from 'rxjs';


describe('ProfilePictureComponent', () => {
  let fixture;
  let component;
  const FirestorageStub = {
    ref: (path: string) => ({
      getDownloadURL: () => new BehaviorSubject({ foo: 'bar' }),
      upload: (path: string, data: any) => new Promise((resolve, reject) => resolve()),
      delete: () => new BehaviorSubject({ foo: 'bar' }),
    }),
  };
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [FormsModule, ReactiveFormsModule],
      declarations: [
        ProfilePictureComponent
      ],
      providers: [
        StorageService,
        { provide: AngularFireStorage, useValue: FirestorageStub }
      ]
    }).overrideComponent(ProfilePictureComponent, {

    }).compileComponents();
    fixture = TestBed.createComponent(ProfilePictureComponent);
    component = fixture.debugElement.componentInstance;
  });

  afterEach(() => {
    component.ngOnDestroy = function () { };
    fixture.destroy();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});