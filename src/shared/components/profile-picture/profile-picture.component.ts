import { StorageService } from './../../../core/services/storage/storage.service';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'app-profile-picture',
  templateUrl: './profile-picture.component.html',
  styleUrls: ['./profile-picture.component.scss']
})
export class ProfilePictureComponent implements OnInit, OnDestroy {
  private unsubscribe: Subject<void> = new Subject();
  // selected image file
  selectedFile: File;
  profileImage: string;
  maxFileSize = 2; // MB
  // blank image path
  blankImage = 'assets/img/blank-profile.png';
  // used to determine the type of image selection
  profileImageExists = false;

  constructor(private fileStorage: StorageService) { }

  ngOnInit() {
    // getting image from storage service
    this.refreshUserImage();
  }

  /**
   * Uploads profile picture to firestorage
   * @param event file input element event
   */
  uploadProfilePicture(event: any) {
    // checking file size
    if (event.target.files[0] === undefined || !this.fileSizeBelowMaxLimit(event.target.files[0])) {
      alert(`File size shouldn\'t be greater then ${this.maxFileSize} MB`);
      return;
    }
    // getting selected file
    this.selectedFile = event.target.files[0];
    const upload = this.fileStorage.uploadUserImage(this.selectedFile);
    // if uploaded shows image as a profile image using the new firestorage path
    upload.then(data => {
      this.refreshUserImage();
    }).catch((error) => {
      console.error('Error on uploading image: ', error);
    });
  }

  /**
   * Checks if the files size is below max limit
   * @param file imaqe file
   * @returns boolean
   */
  fileSizeBelowMaxLimit(file: File) {
    return ((file.size / 1024) < (this.maxFileSize * 1024));
  }

  /**
   * Removes image and replaces it with a default blank image
   */
  removeImage() {
    // should remove image only if image exists
    if (this.profileImageExists !== false) {
      this.fileStorage.removeUserImage();
      this.setProfileImage();
    }
  }

  /**
   * Sets profile image from path
   * @param [imagePath] image path
   */
  setProfileImage(imagePath = this.blankImage) {
    this.profileImage = imagePath;
    this.profileImageExists = this.profileImage !== this.blankImage ? true : false;
  }

  /**
   * Refreshes user image
   */
  refreshUserImage() {
    this.fileStorage.getUserImagePath().pipe(takeUntil(this.unsubscribe))
      .subscribe(data => {
        this.setProfileImage(data);
      }, err => this.setProfileImage());
  }


  ngOnDestroy() {
    console.log('ngOnDestory');
    this.unsubscribe.next();
    this.unsubscribe.complete();
  }
}
