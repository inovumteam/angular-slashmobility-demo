import { Injectable } from '@angular/core';
import { AngularFireStorage, AngularFireUploadTask } from '@angular/fire/storage';
import 'firebase/storage';
import { catchError } from 'rxjs/operators';
import { of, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class StorageService {

  // default user id
  private userId = 'test-user';
  // path to file on firebase firestorage
  private path = `profile-images/${this.userId}`;
  // file reference
  private imageRef = this.fireStorage.ref(this.path);

  constructor(private fireStorage: AngularFireStorage) { }

  /**
   * Gets user image url
   * @returns image path
   */
  getUserImagePath(): Observable<string> {
    return this.imageRef.getDownloadURL();
  }

  /**
   * Uploads user image
   * @param image image file
   * @returns Promise
   */
  uploadUserImage(image: File): AngularFireUploadTask {
    // adding additional data
    const customMetadata = { app: 'Slashmobility user profile image' };
    // uploading image
    return this.fireStorage.upload(this.path, image, { customMetadata });
  }

  /**
   * Removes user image from storage
   * @returns Observable
   */
  removeUserImage(): Observable<any> {
    return this.imageRef.delete();
  }

}
