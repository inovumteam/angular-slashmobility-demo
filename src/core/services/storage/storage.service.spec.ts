import { AngularFireStorage } from '@angular/fire/storage';
import { TestBed } from '@angular/core/testing';

import { StorageService } from './storage.service';
import { BehaviorSubject } from 'rxjs';

describe('StorageService', () => {
  const FirestorageStub = {
    ref: (path: string) => ({
      getDownloadURL: () => new BehaviorSubject({ foo: 'bar' }),
      upload: (uploadPath: string, data: any) => new Promise((resolve, reject) => resolve()),
      delete: () => new BehaviorSubject({ foo: 'bar' }),
    }),
  };
  beforeEach(() => TestBed.configureTestingModule({

    providers: [{ provide: AngularFireStorage, useValue: FirestorageStub }]
  }));

  it('should be created', () => {
    const service: StorageService = TestBed.get(StorageService);
    expect(service).toBeTruthy();
  });
});
