import { Observable } from 'rxjs';
import { User } from './../../../shared/models/user.model';
import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';

@Injectable({
  providedIn: 'root'
})
export class DatabaseService {

  private userCollection = this.fireStore.collection('users');

  private userId = 'test-user';

  private userDocument = this.userCollection.doc<User>(this.userId);

  constructor(private fireStore: AngularFirestore) { }

  /**
   * Gets user from database
   * @returns user Observable<User>
   */
  getUser(): Observable<User> {
    return this.userDocument.valueChanges() as Observable<User>;
  }

  /**
   * Updates user from object
   * @param user User
   * @returns Promise
   */
  updateUser(user: User): Promise<any> {
    return this.userDocument.set(user);
  }

  /**
   * Removes user from database
   * @returns Promise
   */
  removeUser(): Promise<any> {
    return this.userDocument.delete();
  }

}
