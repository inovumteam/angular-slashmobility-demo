import { AngularFirestore } from '@angular/fire/firestore';
import { TestBed } from '@angular/core/testing';

import { DatabaseService } from './database.service';
import { BehaviorSubject } from 'rxjs';

describe('DatabaseService', () => {
  const FirestoreStub = {
    collection: (name: string) => ({
      doc: (id: string) => ({
        valueChanges: () => new BehaviorSubject({ foo: 'bar' }),
        set: (d: any) => new Promise((resolve, reject) => resolve()),
      }),
    }),
  };
  beforeEach(() => TestBed.configureTestingModule({
    providers: [ { provide: AngularFirestore, useValue: FirestoreStub }]
  }));

  it('should be created', () => {
    const service: DatabaseService = TestBed.get(DatabaseService);
    expect(service).toBeTruthy();
  });
});
