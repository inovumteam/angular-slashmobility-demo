import { StorageService } from './../../../core/services/storage/storage.service';
import { MatRadioModule } from '@angular/material/radio';
import { MatInputModule } from '@angular/material/input';
import { MatFormFieldModule } from '@angular/material/form-field';
import { AngularFireStorage } from '@angular/fire/storage';
import { ProfilePictureComponent } from './../../../shared/components/profile-picture/profile-picture.component';
import { User } from './../../../shared/models/user.model';
import { TestBed } from '@angular/core/testing';
import { Directive, Input } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ProfileInformationComponent } from './profile-information.component';
import { FormBuilder } from '@angular/forms';
import { AngularFirestore } from '@angular/fire/firestore';
import { BehaviorSubject } from 'rxjs';
import { MatCardModule } from '@angular/material/card';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

@Directive({ selector: '[appProfileInformation]' })
class OneviewPermittedDirective {
  @Input() oneviewPermitted;
}

describe('ProfileInformationComponent', () => {
  let fixture;
  let component: ProfileInformationComponent;
  const FirestoreStub = {
    collection: (name: string) => ({
      doc: (id: string) => ({
        valueChanges: () => new BehaviorSubject({ foo: 'bar' }),
        set: (d: any) => new Promise((resolve, reject) => resolve()),
        delete: () => new BehaviorSubject({ foo: 'bar' }),
      }),
    }),
  };
  const FirestorageStub = {
    ref: (path: string) => ({
      getDownloadURL: () => new BehaviorSubject({ foo: 'bar' }),
      upload: (uploadPath: string, data: any) => new Promise((resolve, reject) => resolve()),
      delete: () => new BehaviorSubject({ foo: 'bar' }),
    }),
  };
  const validUser: User = { username: 'test1', email: 'test@gmail.com', gender: 'male', bio: 'test' };
  const invalidUser: User = { username: '', email: '', gender: '', bio: '' };


  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        FormsModule,
        ReactiveFormsModule,
        MatFormFieldModule,
        MatInputModule,
        MatRadioModule,
        MatCardModule,
        BrowserAnimationsModule
      ],
      declarations: [
        ProfileInformationComponent,
        ProfilePictureComponent,
        OneviewPermittedDirective
      ],
      providers: [
        FormBuilder,
        { provide: AngularFirestore, useValue: FirestoreStub },
        { provide: AngularFireStorage, useValue: FirestorageStub },
        StorageService
      ]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ProfileInformationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  afterEach(() => {
    fixture.destroy();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });


  it('should validate form with valid values', async () => {
    const spyUserFormReset = spyOn(component.userForm, 'reset');
    component.createForm();
    updateForm(validUser);
    expect(component.userForm.valid).toBeTruthy();
    component.submitForm();
    await Promise.resolve();
    expect(spyUserFormReset).not.toHaveBeenCalled();
  });

  it('should be invalid when empty', () => {
    component.createForm();
    updateForm(invalidUser);
    expect(component.userForm.valid).toBeFalsy();
  });

  it('should be invalid when username is empty', () => {
    const emptyUsernameUser = { ...validUser };
    emptyUsernameUser.username = '';
    component.createForm();
    updateForm(emptyUsernameUser);
    expect(component.userForm.valid).toBeFalsy();
  });

  it('should be invalid when email format is wrong', () => {
    const wrongEmailUser = { ...validUser };
    wrongEmailUser.email = 'wrong';
    component.createForm();
    updateForm(wrongEmailUser);
    expect(component.userForm.valid).toBeFalsy();
  });

  it('should be invalid when gender is not selected', () => {
    const emptyGenderUser = { ...validUser };
    emptyGenderUser.gender = '';
    component.createForm();
    updateForm(emptyGenderUser);
    expect(component.userForm.valid).toBeFalsy();
  });


  it('should be invalid when bio is empty', () => {
    const emptyBioUser = { ...validUser };
    emptyBioUser.username = '';
    component.createForm();
    updateForm(emptyBioUser);
    expect(component.userForm.valid).toBeFalsy();
  });

  function updateForm(user) {
    component.userForm.controls.username.setValue(user.username);
    component.userForm.controls.email.setValue(user.email);
    component.userForm.controls.gender.setValue(user.gender);
    component.userForm.controls.bio.setValue(user.bio);
  }

});
