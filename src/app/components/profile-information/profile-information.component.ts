import { DatabaseService } from './../../../core/services/database/database.service';
import { User } from './../../../shared/models/user.model';
import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import 'firebase/firestore';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'app-profile-information',
  templateUrl: './profile-information.component.html',
  styleUrls: ['./profile-information.component.scss']
})
export class ProfileInformationComponent implements OnInit, OnDestroy {

  @ViewChild('pictureComponent', { static: false }) pictureComponent;

  unsubscribe: Subject<void> = new Subject();

  userForm: FormGroup;

  constructor(private formBuilder: FormBuilder, private database: DatabaseService) { }

  ngOnInit() {
    // creating new form
    this.createForm();
    // if user already created fill form inputs with existing values or reset form if user removed
    this.database.getUser().pipe(takeUntil(this.unsubscribe))
      .subscribe(user => user ? this.userForm.setValue(user) : this.userForm.reset());
  }

  /**
   * Gets email
   */
  get email() {
    return this.userForm.get('email');
  }

  /**
   * Gets username
   */
  get username() {
    return this.userForm.get('username');
  }

  /**
   * Gets gender
   */
  get gender() {
    return this.userForm.get('gender');
  }

  /**
   * Gets bio
   */
  get bio() {
    return this.userForm.get('bio');
  }

  /**
   * Creates form with empty values
   */
  createForm() {
    this.userForm = this.formBuilder.group({
      email: ['', [
        Validators.required,
        Validators.email
      ]],
      username: ['', [
        Validators.required,
      ]],
      gender: ['', [
        Validators.required,
      ]],
      bio: ['', [
        Validators.required,
      ]]
    });
  }

  /**
   * submits form if valid
   */
  submitForm() {
    // if form is valid, create or update user with new information
    if (this.validateForm()) {
      const user: User = this.userForm.value;
      this.database.updateUser(user).then(() => {
        console.log('User data successfully written!');
      }).catch((error) => {
        console.error('Error writing user data: ', error);
      });
    }
  }

  /**
   * Validates form
   * @returns boolean
   */
  validateForm() {
    if (this.userForm.invalid) {
      // mark all inputs as touched to show validation errors
      this.userForm.markAllAsTouched();
      this.gender.markAsTouched();
      return false;
    }
    return true;
  }

  /**
   * Resets user information
   */
  resetUser() {
    this.pictureComponent.removeImage();
    this.database.removeUser().then(() => {
      console.log('User information removed');
    }).catch((error) => {
      console.error('Error writing user data: ', error);
    });
  }

  ngOnDestroy() {
    console.log('ngOnDestory');
    this.unsubscribe.next();
    this.unsubscribe.complete();
  }
}
