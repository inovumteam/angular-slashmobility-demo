import { LocationService } from '../../../core/services/location/location.service';
import { Component, AfterViewInit, ViewChild, ElementRef } from '@angular/core';
@Component({
  selector: 'app-profile-location',
  templateUrl: './profile-location.component.html',
  styleUrls: ['./profile-location.component.scss']
})
export class ProfileLocationComponent implements AfterViewInit {

  @ViewChild('mapContainer', { static: false }) gmap: ElementRef;

  map: google.maps.Map;

  constructor(private locationService: LocationService) { }

  /**
   * Maps initializer
   */
  mapInitializer() {
    // default initial values
    const coordinates = new google.maps.LatLng(41.3887901, 2.1589899);
    // map options definition
    const mapOptions: google.maps.MapOptions = {
      center: coordinates,
      zoom: 2,
    };
    // initialize map with default values
    this.map = new google.maps.Map(this.gmap.nativeElement, mapOptions);
  }

  /**
   * Shows my location on map
   */
  showMyLocation() {
    // getting current location with Location Service
    this.locationService.getPosition().then(pos => {
      // defining current location coordinates
      const coordinates = new google.maps.LatLng(pos.lat, pos.lng);
      // creating a new marker
      const marker = new google.maps.Marker({
        position: coordinates,
        map: this.map,
      });
      // defining options with new coordinates and zoom
      const mapOptions: google.maps.MapOptions = {
        center: coordinates,
        zoom: 8,
      };
      // replacing old map with a new one with new values
      this.map = new google.maps.Map(this.gmap.nativeElement, mapOptions);
      // creating a window to show information about current location
      const window = new google.maps.InfoWindow({
        content: '<b>Latitude: </b>' + pos.lat + '<br><b>Longitude: </b>' + pos.lng
      });
      // setting the marker on map
      marker.setMap(this.map);
      // opening window with information
      window.open(this.map, marker);
    });
  }

  ngAfterViewInit() {
    // shows initial map
    this.mapInitializer();
  }
}
