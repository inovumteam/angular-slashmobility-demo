import { ProfileLocationComponent } from './profile-location.component';
import { TestBed } from '@angular/core/testing';
import { Injectable, CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA, Directive, Input } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LocationService } from '../../../core/services/location/location.service';

@Injectable()
class MockLocationService { }

@Directive({ selector: '[appProfileLocation]' })
class OneviewPermittedDirective {
    @Input() oneviewPermitted;
}

describe('ProfileLocationComponent', () => {
    let fixture;
    let component;

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [FormsModule, ReactiveFormsModule],
            declarations: [
                ProfileLocationComponent,
                OneviewPermittedDirective
            ],
            schemas: [CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA],
            providers: [
                { provide: LocationService, useClass: MockLocationService }
            ]
        }).overrideComponent(ProfileLocationComponent, {

        }).compileComponents();
        fixture = TestBed.createComponent(ProfileLocationComponent);
        component = fixture.debugElement.componentInstance;
    });

    afterEach(() => {
        fixture.destroy();
    });

    it('should be created', () => {
        expect(component).toBeTruthy();
    });

});
