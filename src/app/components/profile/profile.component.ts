import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {

  breakpoint = null;

  constructor() { }

  ngOnInit() {
    this.breakpoint = (window.innerWidth <= 980) ? 1 : 2;
  }

  /**
   * On resize window changes number of grid columns
   */
  onResize(event) {
    this.breakpoint = (event.target.innerWidth <= 980) ? 1 : 2;
  }
}
