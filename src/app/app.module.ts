import { UiModule } from './ui.module';

import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { SharedModule } from '../shared/shared.module';
import { AppComponent } from './components/app.component';
import { ProfileComponent } from './components/profile/profile.component';
import { ProfileLocationComponent } from './components/profile-location/profile-location.component';
import { ProfileInformationComponent } from './components/profile-information/profile-information.component';
import { ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';

@NgModule({
  declarations: [
    AppComponent,
    ProfileComponent,
    ProfileLocationComponent,
    ProfileInformationComponent
  ],
  imports: [
    CommonModule,
    BrowserModule,
    SharedModule,
    UiModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
