# Angular Slashmobility Demo #
This project emulates a CMS profile section, that includes a user form with validations and a geolocation detection.
For simplicity a Online Firebase Database is used to store the user data. 
A demo is deployed on: https://www.inovum-solutions.com/slashmobility 

## Built With
* [Google Maps API](https://cloud.google.com/maps-platform/) - Used to show a user's geolocation on the map.
* [Firebase](https://firebase.google.com/) - Provides database and file storage.
* [Angular 8.2](https://angular.io/) - The web frontend framework used
* [Angular CLI 8.3](https://cli.angular.io/) - Angular command line interface.
* [Node ^10.16.2](https://nodejs.org) - Runtime environment used by Angular
* [npm 6.4.1](https://www.npmjs.com/) - Package manager
* [git](https://git-scm.com) - Version control system

## Prerequisites
* Google Maps API key
* Firebase project credentials
* node ^10.16.2
* Angular 8.2
* Angular CLI 8.3
* npm 6.4.1

## Project setup and configuration ##
1. Clone project to your local machine
2. Add your Google Maps API key to index.html
3. Add your firebase project credentials to environment.ts and environment.prod.ts
4. Run 'npm i' to install all node modules
5. Run  'ng serve' for a dev server and navigate to http://localhost:4200/

## Build
Run `ng build` to build the project. The build files will be stored in the `dist/` directory. 
Use the `-prod` flag for a production build.

## Running unit tests
Run `ng test` to execute the unit tests via [Karma]

## Firebase configuration
Add your firebase project configuration to environment.ts and environment.prod.ts files

## Google Maps API configuration
Add your Google Maps API key to index.html
